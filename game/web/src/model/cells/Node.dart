part of game;

class Node extends Cell {

  Node(Point<int> pos) : super(pos) {
    this.addEventListener(MouseEvent.CLICK, _onClick);
  }

  void _onClick(MouseEvent me) {
    print("Click!");
  }

  void addTo(World world) {
    this.world = world;
    world.put(this);
  }

}
