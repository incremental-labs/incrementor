part of game;

const diameter = 10;
const scale = 0.9;

class World extends Sprite with Animatable {
  Map<Point<int>, Cell> map;
  Juggler juggler;

  World(){
    map = {};
    juggler = new Juggler();
    Terminal terminal = new Terminal();
    terminal.addTo(this);
  }

  bool advanceTime(num time) {
    return this.juggler.advanceTime(time);
  }

  Cell get(Point<int> pos) => map[pos];

  Cell put(Cell cell) {
    Cell prev = map[cell.pos];
    map[cell.pos] = cell;
    if(prev != null){
      this.removeChild(prev);
      juggler.remove(prev);
    }
    if(cell != null){
      this.addChild(cell);
      cell.setTransform(cell.x, -cell.y);
      juggler.add(cell);
    }
    return prev;
  }

  Cell remove(Cell cell) {
    Cell prev = map[cell.pos];
    if (prev == cell) {
      map.remove(cell.pos);
      this.removeChild(prev);
      juggler.remove(prev);
    }
    return prev;
  }

}

class Cell extends Sprite with Animatable {
  World world;
  Juggler juggler;

  Point<int> pos;
  num x, y;

  Sprite sprite;

  List<Point<int>> _neighbors;

  List<Point<int>> get neighbors {
    if(_neighbors == null){
      _neighbors = within(1);
    }
    return _neighbors;
  }

  Cell(this.pos, {this.juggler : null, this.sprite : null}) {
    x = diameter * 3 / 2 * pos.x;
    y = diameter * SQRT_3 * (pos.y + (pos.x / 2));
    if(juggler == null){
      juggler = new Juggler();
    }
    if(sprite == null){
      sprite = new Hexagon(diameter);
    }
    this.addChild(sprite);
  }

  addTo(World world){
    this.world = world;
    world.put(this);
    neighbors.map((Point<int> pos) {
      if(world.get(pos) == null){
        Node n = new Node(pos);
        n.addTo(world);
        return n;
      } else {
        return world.get(pos);
      }
    });
  }

  List<Point<int>> within(int steps) {
    List<Point<int>> result = [];
    for(int q = -steps; q <= steps; q++){
      for(int r = max(-steps, -q - steps); r <= min(steps, -q + steps); r++){
        result.add(new Point<int>(pos.x + q, pos.y + r));
      }
    }
    return result;
  }

  bool advanceTime(num time) {
    this.juggler.advanceTime(time);
  }

}