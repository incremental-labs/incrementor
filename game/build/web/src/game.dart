library game;

import 'dart:html';
import 'dart:math' hide Point;
import 'package:stagexl/stagexl.dart';

part 'event/event.dart';
part 'geom/hexagon.dart';
part 'model/model.dart';
part 'model/cells/Terminal.dart';
part 'model/cells/Node.dart';
part 'ui/viewport.dart';

final SQRT_3 = sqrt(3);
final SQRT_3_2 = SQRT_3 / 2;

final velocity = 50.0;

class Game extends DisplayObjectContainer {

  Stage stage;
  Viewport viewport;
  World world;

  Game(this.stage) {
    this.viewport = new Viewport(stage);
    this.world = new World();
    viewport.addChild(world);
    stage.addChild(viewport);
    stage.juggler.add(viewport);
    stage.juggler.add(world);
    
    stage.focus = stage;
  }

}
