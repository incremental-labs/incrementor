part of game;

class Viewport extends Sprite implements Animatable {

  static const int LEFT_ARROW = 37;
  static const int RIGHT_ARROW = 39;
  static const int UP_ARROW = 38;
  static const int DOWN_ARROW = 40;

  Stage stage;
  Point<num> get center {
    return new Point<num>(stage.sourceWidth / 2, stage.sourceHeight / 2);
  }

  Juggler get juggler {
    return stage.juggler;
  }

  num vx = 0,
      vy = 0;

  Viewport(this.stage) {
    this.setTransform(center.x, center.y, scale, scale);
    stage.onKeyDown.listen((e) => _onKeyPressed(e));
    stage.onKeyUp.listen((e) => _onKeyPressed(e));
  }

  void _onKeyPressed(KeyboardEvent ke) {
    switch (ke.type) {
      case KeyboardEvent.KEY_DOWN:
        switch (ke.keyCode) {
          case LEFT_ARROW:
            vx = velocity;
            break;
          case UP_ARROW:
            vy = velocity;
            break;
          case RIGHT_ARROW:
            vx = -velocity;
            break;
          case DOWN_ARROW:
            vy = -velocity;
            break;
        }
        break;
      case KeyboardEvent.KEY_UP:
        switch (ke.keyCode) {
          case LEFT_ARROW:
            vx = 0;
            break;
          case UP_ARROW:
            vy = 0;
            break;
          case RIGHT_ARROW:
            vx = 0;
            break;
          case DOWN_ARROW:
            vy = 0;
            break;
        }
        break;
    }
  }

  bool advanceTime(num time) {
    num tx = x + (vx * time);
    num ty = y + (vy * time);
    this.setTransform(tx, ty);
    return true;
  }

}
