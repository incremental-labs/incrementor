part of game;

class Hexagon extends Sprite {

  static final START = new Point<num>(1, -SQRT_3);
  static final VERTICES = <Point<num>>[new Point<num>(2, 0), new Point<num>(1, SQRT_3), new Point<num>(-1, SQRT_3), new Point<num>(-2, 0), new Point<num>(-1, -SQRT_3), new Point<num>(1, -SQRT_3)];

  num diameter;

  int stroke, fill;

  Shape shape;
  Graphics graphics;

  Hexagon(this.diameter, {this.stroke: Color.Black, this.fill: Color.Gray}) {
    // init shape display object
    shape = new Shape();
    graphics = shape.graphics;

    // init hexagon graphics
    graphics
      ..beginPath()
      ..moveTo(0, 0)
      ..moveTo(START.x * diameter / 2, START.y * diameter / 2);
    VERTICES.forEach((vertex) => graphics.lineTo(vertex.x * diameter / 2, vertex.y * diameter / 2));
    graphics
      ..moveTo(0, 0)
      ..closePath();

    // color hexagon graphics
    graphics.fillColor(fill);
    graphics.strokeColor(stroke);

    // add hexagon to display list
    this.addChild(shape);

    this.onMouseClick.listen(_onMouseClick);
    this.onMouseOver.listen(_onMouseOver);
    this.onMouseOut.listen(_onMouseOut);

  }

  void _onMouseClick(MouseEvent me) {
    // override
  }

  void _onMouseOver(MouseEvent me) {
    graphics.fillColor(Color.White);
  }

  void _onMouseOut(MouseEvent me) {
    graphics.fillColor(Color.Gray);
  }

}
