part of game;

class CellEvent extends Event {

  static const CLICK = "click";
  Cell source;

  CellEvent(String type, this.source) : super(type);

}

class ClickEvent extends CellEvent {

  ClickEvent(Cell source): super(CellEvent.CLICK, source);

}