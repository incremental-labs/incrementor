import 'dart:html';
import 'package:stagexl/stagexl.dart';
import 'src/game.dart';

final CanvasElement canvas = querySelector('#stage');
final Stage stage = new Stage(canvas);
final RenderLoop renderLoop = new RenderLoop();

void main() {
  renderLoop.addStage(stage);
  new Game(stage);
}
